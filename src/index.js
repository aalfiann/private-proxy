'use strict'

const net = require('net')
const server = net.createServer()

function dateToISOWithTimezone (datestring) {
  const dt = (datestring ? new Date(datestring) : new Date())
  const tzo = -dt.getTimezoneOffset()
  const dif = tzo >= 0 ? '+' : '-'
  return dt.getFullYear() +
    '-' + ('0' + (dt.getMonth() + 1)).slice(-2) +
    '-' + ('0' + dt.getDate()).slice(-2) +
    'T' + ('0' + dt.getHours()).slice(-2) +
    ':' + ('0' + dt.getMinutes()).slice(-2) +
    ':' + ('0' + dt.getSeconds()).slice(-2) +
    dif + ('0' + Math.floor(Math.abs(tzo) / 60)).slice(-2) +
    ':' + ('0' + Math.abs(tzo) % 60).slice(-2)
}

function writeLog(text) {
  return console.log(dateToISOWithTimezone() + ' >> ' + text)
}


server.on('connection', (clientToProxySocket) => {
  writeLog('Client request new proxy connection...')
  // start packet only once
  clientToProxySocket.once('data', (data) => {
    let isTLSConnection = data.toString().indexOf('CONNECT') !== -1

    // By Default port is 80
    let serverPort = 80
    let serverAddress
    if (isTLSConnection) {
      // Port changed if connection is TLS
      serverPort = data.toString().split('CONNECT ')[1].split(' ')[0].split(':')[1]
      serverAddress = data.toString().split('CONNECT ')[1].split(' ')[0].split(':')[0]
    } else {
      serverAddress = data.toString().split('Host: ')[1].split('\r\n')[0]
    }

    writeLog('Accept new proxy connection...')

    let proxyToServerSocket = net.createConnection({
      host: serverAddress,
      port: serverPort
    }, () => {
      writeLog('Sending proxy request to ' + serverAddress)
      if (isTLSConnection) {
        clientToProxySocket.write('HTTP/1.1 200 OK\r\n\n')
      } else {
        proxyToServerSocket.write(data)
      }

      clientToProxySocket.pipe(proxyToServerSocket)
      proxyToServerSocket.pipe(clientToProxySocket)

      proxyToServerSocket.on('timeout', (err) => {
        writeLog('SOCKET SERVER TIMEOUT')
        writeLog(err)
      });

      proxyToServerSocket.on('error', (err) => {
        writeLog('SOCKET SERVER ERROR')
        writeLog(err)
      });
      
    });
    clientToProxySocket.on('error', err => {
      writeLog('Failed connecting to socket server')
      writeLog(err)
    })
  })
})

server.on('error', (err) => {
  writeLog('SOCKET SERVER ERROR')
  writeLog(err)
  throw err
})

server.on('close', () => {
  writeLog('Client Disconnected')
})

server.listen(8124, () => {
  writeLog('Proxy running at http://localhost:' + 8124)
})
