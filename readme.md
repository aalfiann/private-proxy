# Private Proxy
Simple Private Proxy for Personal Use.  
This was created use official NodeJS API, no any dependencies needed.

### Usage
1. Run the server proxy
```bash
$ npm start
```

2. Setting your browser to use proxy, and fill all like this picture below.
![](./settings.png)

3. Done

**Note:**
- Actualy this private proxy can be used for any applications (not only browser).
- This private proxy is designed as simple as less configuration for personal use only.

### Support Me
I've accept cryptocurrency $XEC #eCash  
![](https://i.imgur.com/QCGuEYj.png)  
ecash:qqcn5j6ej57xngsscn7ugm80un4jujk72qypcm8gyl


### MIT License
Copyright 2023 M ABD AZIZ ALFIAN

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
